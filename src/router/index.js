import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const Home = () => import(/* webpackChunkName: "home" */ "../views/Home.vue");
const Disburse = () =>
  import(/* webpackChunkName: "disburse" */ "../views/Disburse.vue");
const Login = () =>
  import(/* webpackChunkName: "login" */ "../views/Login.vue");

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/login",
    name: "Login",
    component: Login,
    meta: {
      guestOnly: true
    }
  },
  {
    path: "/disburse",
    name: "Disburse",
    // route level code-splitting
    // this generates a separate chunk (disburse.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Disburse,
    meta: {
      requiresAuth: true
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
