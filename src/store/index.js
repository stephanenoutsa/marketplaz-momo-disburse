import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import router from "../router";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    acctBalance: 0
  },
  getters: {
    loggedIn: state => state.loggedIn,
    acctBalance: state => state.acctBalance
  },
  mutations: {
    SET_USER_DATA(state, { ...userData }) {
      state.loggedIn = true;
      localStorage.setItem("mk-user", JSON.stringify(userData));
      axios.defaults.headers.common[
        "Authorization"
      ] = `Bearer ${userData.token}`;
    },

    UNSET_USER_DATA(state) {
      state.loggedIn = false;
      localStorage.removeItem("mk-user");
      axios.defaults.headers.common["Authorization"] = "";

      router.push({ name: "Home" });
    },

    SET_ACCT_BALANCE(state, balance) {
      state.acctBalance = balance;
    },

    INCREMENT_ACCT_BALANCE(state, amount) {
      state.acctBalance += amount;
    },

    DECREMENT_ACCT_BALANCE(state, amount) {
      if (state.acctBalance >= amount) {
        state.acctBalance -= amount;
      }
    }
  },
  actions: {
    logout(context) {
      context.commit("UNSET_USER_DATA");
    }
  },
  modules: {}
});
