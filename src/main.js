import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

Vue.config.productionTip = false;

// Navigation guards before each request
router.beforeResolve((to, from, next) => {
  const loggedIn = localStorage.getItem("mk-user");

  if (to.matched.some(record => record.meta.requiresAuth) && !loggedIn) {
    next("/login");
  } else if (to.matched.some(record => record.meta.guestOnly) && loggedIn) {
    next("/");
  }

  next();
});

/**
 * Disable debugging when in production
 */
if (process.env.NODE_ENV === "production") {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

new Vue({
  router,
  store,
  created() {
    // Automatic Login
    const userString = localStorage.getItem("mk-user");

    if (userString) {
      const userData = JSON.parse(userString);
      this.$store.commit("SET_USER_DATA", userData);
    }

    // Security measure
    axios.interceptors.response.use(
      response => response,
      error => {
        if (error.response.status === 401) {
          this.$store.dispatch("logout");
        }

        return Promise.reject(error);
      }
    );
  },
  render: h => h(App)
}).$mount("#app");
