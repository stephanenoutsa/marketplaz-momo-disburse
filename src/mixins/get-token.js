import axios from "axios";

export default {
  methods: {
    async getToken() {
      // const url = process.env.VUE_APP_MOMO_BASE_URL + "/token/";
      const url = process.env.VUE_APP_API_URL + "/token";

      // const data = {};
      const data = {
        subscription_key: process.env.VUE_APP_MOMO_SUBSCRIPTION_KEY,
        api_user: process.env.VUE_APP_MOMO_API_USER,
        api_key: process.env.VUE_APP_MOMO_API_KEY
      };

      // const config = {
      //   auth: {
      //     username: process.env.VUE_APP_MOMO_API_USER,
      //     password: process.env.VUE_APP_MOMO_API_KEY
      //   },

      //   headers: {
      //     "Ocp-Apim-Subscription-Key": process.env.VUE_APP_MOMO_SUBSCRIPTION_KEY
      //   }
      // };
      const config = {};

      const response = await axios.post(url, data, config);

      if (response.status == 200) {
        return response.data.access_token;
      }

      return null;
    }
  }
};
