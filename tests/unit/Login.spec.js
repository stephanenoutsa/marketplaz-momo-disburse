import Vue from "vue";
import { shallowMount } from "@vue/test-utils";
import Login from "@/views/Login.vue";
import axios from "axios";
import flushPromises from "flush-promises";

jest.mock("axios");

const createConfig = overrides => {
  const id = 1;
  const mocks = {
    // Vue Auth
    $auth: {
      check: () => false
    },
    // Vue Router
    $router: {
      push: () => {}
    },
    // Vuex
    $store: {
      state: [{ id }],
      commit: () => {}
    }
  };
  const propsData = { id };

  const data = () => {
    return {
      //
    };
  };

  return Object.assign({ mocks, propsData, data }, overrides);
};

beforeEach(() => {
  //
});

describe("Login.vue", () => {
  it("logs in user", async () => {
    const data = () => {
      return {
        username: "test",
        password: "test"
      };
    };

    const response = {
      data: {
        token: "token",
        user_email: "user_email",
        user_nicename: "user_nicename",
        user_display_name: "user_display_name"
      }
    };

    const config = createConfig({ data });

    const wrapper = shallowMount(Login, config);

    // const spy = jest.spyOn(config.mocks.$router, "push");
    const spy = jest.spyOn(axios, "get");

    expect(wrapper.vm.username).toBe("test");

    wrapper.vm.submit();

    flushPromises();

    await Vue.nextTick();

    axios.get.mockResolvedValue(response);

    expect(spy).toHaveBeenCalledTimes(1);
  });
});
